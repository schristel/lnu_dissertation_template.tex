\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Publications}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Included publications}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Author's contributions}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Publications not discussed in this thesis}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Abbreviations}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Introduction}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Acidophiles}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Diversity of acidophilic prokaryotes}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bacteria}{20}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Proteobacteria}{22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Nitrospirae}{23}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Firmicutes}{24}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Actinobacteria}{25}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Aquificae and Verrucomicrobia}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Archaea}{26}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Crenarchaeota}{26}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Euryarchaeota}{28}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Energy and carbon metabolism}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Iron}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Inorganic sulfur compounds}{33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Hydrogen}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Carbon}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Challenges and adaptations of life in acid}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{pH}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Heavy metals}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Oxidative stress}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Polyextremophiles}{43}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Temperature: Psychro- and thermoacidophiles}{43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Environmental and ecological implications of acidophiles}{46}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Acid rock and mine drainage}{46}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Acid sulfate soils}{50}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Volcanic and geothermal environments}{51}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Applications of acidophiles}{54}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Biomining}{54}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Chalcopyrite bioleaching}{57}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Biomining in boreal climates}{58}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Bioprospecting and genetic engineering}{59}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Aims of this Thesis}{61}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Methodology}{63}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Experiments and sampling}{63}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Sequencing}{65}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Multi "-omics" analysis}{65}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Summary of Results}{67}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Papers I-IV: Systems biology of biomining organisms}{69}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Physiology of cold-adapted \textit {Acidithiobacillus ferrivorans}}{69}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Physiology of iron-oxidizer \textit {Leptospirillum ferriphilum}}{70}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Community-controlled leaching of chalcopyrite}{71}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{Paper V: Acid sulfate soil in Sweden}{72}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Conclusions}{75}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Acknowledgements}{77}
\defcounter {refsection}{0}\relax 
\contentsline {section}{References}{81}
