All studies in this thesis required either experimental work in the laboratory or field sampling. Each project also included extensive nucleic acid and to a lesser extent, protein sequencing (Table \ref{tab:methods}). Sequence data analysis and interpretation were performed as final steps before preparing the manuscripts. A methodology in brief can be found in the following sections. For more detailed descriptions of the used materials and methods the reader is referred to the respective papers.

\renewcommand{\arraystretch}{1.2}
\begin{sidewaystable}[p]
	\centering	
	\begin{threeparttable}
	\caption{Summary of experimental conditions and sequencing technologies used in the studies included in this thesis. }
		\begin{tabular}{@{}llllll@{}}
				\toprule
				\textbf{Paper} & \textbf{Organism}       & \textbf{Cultivation} & \textbf{Substrate}     & \textbf{Temperature (\degree C)} & \textbf{Sequencing} \\ \midrule
				I              & \textit{At. ferrivorans}         & Continuous           & \ce{S4O6^{2-}}           & 20                                                                         & Transcriptome \vspace{.5cm} \\
				II             & \textit{At. ferrivorans}         & Continuous           & \ce{S4O6^{2-}}          & 8, 20                                                                    & Transcriptome \vspace{.5cm} \\
				III            & \textit{L. ferriphilum}          & Batch                & Fe\textsuperscript{2+}                     & 37                                                                         & Genome        \vspace{.5cm} \\
				&                         & Continuous           & Fe\textsuperscript{2+}                     & 37                                                                         & Transcriptome, Proteome  \\
				&                         &                      &                        &                                                                            &              \\
				&                         & Batch                & CuFeS\textsubscript{2} & 37                                                                         & Transcriptome, Proteome  \\
				&                         &                      &                        &                                                                            &               \vspace{.5cm} \\
				IV             & \textit{At. caldus}              & Batch                & CuFeS\textsubscript{2} & 37                                                                         & Transcriptome  \\
				& \textit{L. ferriphilum}          &                      &                        &                                                                            &                     \\
				& \textit{S. thermosulfidooxidans} &                      &                        &                                                                            &                     \vspace{.5cm} \\
				V              & Environmental community & Field sampling       &                        &                                                                            & 16S rRNA gene   \\ \bottomrule
		\end{tabular}
	\label{tab:methods}
	\end{threeparttable}
\end{sidewaystable}
	
%\end{table}

\subsection{Experiments and sampling}
	\textit{\Gls{at.} ferrivorans} sulfur oxidation and cold adaptation (\textbf{I, II}) were assessed via two tetrathionate-fed continuous cultivation experiments, run for several weeks at 8 and 20 \degree C, respectively. The two growth conditions were sampled in duplicate and rapidly cooled to ensure isolation of a representative transcriptome. Cells were then pelleted by centrifugation and lysed using Tri-reagent (Ambion) according to the manufacturer's recommendations. The lysate was cleaned by addition of bromo-chloropropane, followed by centrifugation with retention of the supernatant. Genomic DNA was removed applying the Turbo DNA-free Kit (Ambion).
	
	\textit{\Gls{l.} ferriphilum} (\textbf{III}) was maintained both in batch mode and by continuous cultivation, using ferrous sulfate as electron donor at 37 \degree C. The Genomic-tip 100/G extraction kit (Quiagen) was applied to obtain uninterrupted genomic DNA suitable for ultra long-read genome sequencing from a single sample from the batch culture. Samples for RNA plus protein isolation were taken in triplicate from the continuous cultures. They were rapidly cooled and cells were harvested by centrifugation. Extraction of RNA and proteins was initiated by cell lysis using cryo-milling and bead beating, followed by application of the Allprep isolation kit (Quiagen).
	
	Bioleaching experiments (\textbf{III, IV}) were conducted in shaking flasks using chalcopyrite concentrate. Quadruplet cultures inoculated singly or with combinations of the biomining model species \textit{\Gls{at.} caldus}, \textit{\Gls{l.} ferriphilum}, and \textit{\Gls{s.} thermosulfidooxidans} were incubated at 38 \degree C. Experiments were analyzed for pH, \gls{orp}, \ce{Fe^{2+}}, dissolved and elemental sulfur, as well as total iron and copper until 14 days after onset of microbial activity. Thereafter, planktonic cells were harvested from the medium by centrifugation and RNA and proteins were extracted as described above for \textit{\Gls{l.} ferriphilum}.
	
	The investigated \glspl{ass} (\textbf{V}) were sampled from six sites in Västerbotten county (Sweden). The soil profile's pH was measured, samples of the different horizons subsequently dried, and analyzed for heavy metal and sulfur content by inductive coupled plasma mass and optical emission spectrometry, respectively. Bacterial soil communities were analyzed by separation of intact cells from the soil and subsequent extraction of metagenomic DNA using the PowerSoil DNA Isolation Kit (MoBio). V3-V5 regions of the 16S rRNA gene were amplified and uniquely labeled for sequencing. 
	
\subsection{Sequencing}	
	All nucleic acid sequencing conducted during the work on this thesis was performed by the Science for Life Laboratory (SciLife; Stockholm, Sweden), including library preparations.	
	A complete, circular genome sequence of \textit{\Gls{l.} ferriphilum} (\textbf{III}) was obtained using two Pacific BioSciences (PacBio) single-molecule real-time sequencing cells. 16S rRNA gene amplicon sequencing for community analysis of \gls{ass} (\textbf{V}) was performed using the Illumina MiSeq v3 platform.	
	Total and depleted RNA transcript sequences (\textbf{I, II, III, IV}) were obtained using an Illumina HiSeq2500 in high output mode.
	Protein sequences (\textbf{III}) were obtained by collaborators using an EASY-nLC 1000 liquid chromatography system (Thermo Scientific), and a Q-Exactive HF mass spectrometer (Thermo Scientific).
	
\subsection{Multi "-omics" analysis}
	Various bioinformatic tools were used to analyze sequencing data. The \textit{\Gls{l.} ferriphilum} genomic reads obtained from PacBio ultra long-read sequencing (\textbf{III}) were assembled at the sequencing facility by HGAP3. The resulting circular contig was annotated by Prokka \parencite{Seemann2014} using the standard plus a custom database including genes from related biomining organisms from the Integrated Microbial Genomes system \parencite{Markowitz2012}.
	16S rRNA gene amplicons (\textbf{V}) were analyzed through the UPARSE pipeline \parencite{Edgar2013} and \glspl{otu} annotated against the SILVA database \parencite{Quast2013}. Data analysis was conducted in R using the phyloseq package \parencite{McMurdie2013}.
	
	\textit{\Gls{at.} ferrivorans} transcriptomic reads (\textbf{I, II}) were processed using its published genome sequence as a reference and the "Tuxedo" pipeline \parencite{Trapnell2012}, including differential expression analysis of the two temperature conditions (\textbf{II}). For (meta-) transcriptome analysis of \textit{\Gls{l.} ferriphilum} (\textbf{III}) and the bioleaching experiments (\textbf{IV}), sequencing reads were mapped to the respective reference genomes, and differential expression analysis was performed using the DESeq2 package in R \parencite{Love2014}.
	
	Protein identification for \textit{\Gls{l.} ferriphilum} (\textbf{III}) was performed using the Andromeda software \parencite{Cox2011}. Perseus \parencite{Tyanova2016} aided in differential protein abundance assessment and statistical analysis.
	
	
	 
